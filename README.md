# rabbit_client

Проект для работы с Rabbit, при помощи библиотеки pika

## Установка

1. Создайте папку для проекта:
```
mkdir rabbit
cd rabbit
```
2. Склонируйте репозиторий: 
```
git clone https://gitlab.com/VBolshakovsky/rabbit.git
```
3. Создаем виртуальное окружение
Windows:
```
python -m venv venv
```
Linux:
```
python3 -m venv venv
```
3. Активируем виртуальное окружение
Windows:
```
venv\Scripts\Activate.ps1
```
Linux:
```
source env/bin/activate
```
3. Обновим pip
```
python -m pip install --upgrade pip
```
4. Установите необходимые пакеты: 
```
pip install -r .\rabbit\requirements\base.txt
```
5. Запустите RabbitMQ в докере
```
docker compose -f "rabbit\docker-compose.yml" up -d --build
```

## Использование

rabbit\src\service\rabbit\example.py

## Лицензия

No license file
