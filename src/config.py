from os import environ
from dotenv import load_dotenv

load_dotenv()


class Settings:
    #Общие
    APP_NAME = environ.get('APP_NAME', 'rabbit_client')
    TIME_ZONE = environ.get('TIME_ZONE', 'Europe/Moscow')

    #Rabbit
    RMQ_HOST = environ.get('RMQ_HOST', 'localhost')
    RMQ_PORT = environ.get('RMQ_PORT', 5672)
    RMQ_USER = environ.get('RMQ_USER', 'guest')
    RMQ_PASSWORD = environ.get('RMQ_PASSWORD', 'guest')
