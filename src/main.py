from src.service.rabbit.instance import rabbit

r = rabbit()

r.create_structure(exchange_name="test.exchange", queues=[
    "test.queue",
])

print(
    r.publish_message(
        exchange="test.exchange",
        message={
            "id": 637244,
            "guid": "8812E896-DDBF-40E2-8FC3-B3E497B6C665",
            "type_change": "edit_client",
            "body": {
                "LONG_NAME": "ТЕСТ12 АННА СЕРГЕЕВНА",
                "TIME_EDIT": "2023-06-28T10:23:17.800000",
                "items_changes": []
            }
        },
        #reply_to="exchange.response",
        message_type="event"))