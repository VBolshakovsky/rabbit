"""
Description: Create instance RabbitMQ
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 14.05.2023
Links:
    https://pika.readthedocs.io/en/stable/
    https://www.rabbitmq.com/
    Примеры официальные pika
    https://github.com/pika/pika/tree/main/examples
    Статьи на habr
    https://habr.com/ru/articles/488654/
Comment:
"""

from client import RabbitMQ
from src.config import Settings as settings


def rabbit() -> RabbitMQ:
    """
    Функция, которая создает экземпляр класса Rabbit
    """
    return RabbitMQ(host=settings.RMQ_HOST,
                    port=settings.RMQ_PORT,
                    username=settings.RMQ_USER,
                    password=settings.RMQ_PASSWORD)