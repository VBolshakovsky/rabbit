"""
Description: Examples of using for Rabbit
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 14.05.2023
Links:
    https://pika.readthedocs.io/en/stable/
    https://www.rabbitmq.com/
    Примеры официальные pika
    https://github.com/pika/pika/tree/main/examples
    Статьи на habr
    https://habr.com/ru/articles/488654/
Comment:
"""

from client import RabbitMQ

r = RabbitMQ()
#Basic example:

r.create_structure(exchange_name="test.exchange", queues=[
    "test.queue",
])

print(
    r.publish_message(
        exchange="test.exchange1",
        message={
            "id": 637244,
            "guid": "8812E896-DDBF-40E2-8FC3-B3E497B6C665",
            "type_change": "edit_client",
            "body": {
                "LONG_NAME": "ТЕСТ12 АННА СЕРГЕЕВНА",
                "TIME_EDIT": "2023-06-28T10:23:17.800000",
                "items_changes": []
            }
        },
        #reply_to="exchange.response",
        message_type="event"))

# #Extended example
# # Ограничим колличество сообщений в рамках соединения до 10
# r.prefetch_count = 10
# r.qos()

# # Создадим exchange(роутер)
# print(r.exchange_create('test.exchange', exchange_type='fanout'))
# # Создадим queue(очередь)
# print(r.queue_create('test.queue'))
# # Свяжем(bind) queue и exchange
# print(r.queue_bind('test.queue', 'test.exchange', ''))
# # Проверим существование exchange(роутер)
# print(r.exchange_exist('test.exchange'))
# # Проверим существование queue(очередь)
# print(r.queue_exist('test.queue'))
# # Опубликуем сообщения
# print(r.publish_message('test.exchange', 'Hello!', app_id="test_app"))
# print(r.publish_message('test.exchange', 'World!', app_id="test_app"))
# # Получим колличество сообщений в очереди
# print(r.queue_message_count('test.queue'))
# # Получим сообщение с подтверждением получения(удалиться из очереди)
# message_all = (r.get_message_all('test.queue', auto_ack=True))
# for message in message_all:
#     print('body:' + str(message['body']) + ' ' + 'delivery_tag:' +
#           str(message['method_frame'].delivery_tag))
# # Опубликуем еще сообщение
# print(r.publish_message('test.exchange', 'Hello world!', app_id="test_app"))
# # Получим сообщение без подтверждения(не удалится из очереди)
# method_frame, header_frame, body = r.get_message('test.queue', auto_ack=False)
# print(body)
# # Отправим подтверждение получения по delivery_tag(удалиться из очереди)
# r.ack_message(delivery_tag=method_frame.delivery_tag)
# # Опубликуем еще сообщение
# print(r.publish_message('test.exchange', 'World, Hello!', app_id="test_app"))

# #Функция обработчик
# def callback(ch, method, properties, body):
#     print("Received message:", body)

# tag = (r.consume(queue_name="test.queue", callback=callback))
# # Отпишемся от очереди
# r.consume_unsubscribe(tag)
# # Оформим подписку на очередь
# # Запустим прослушивание очереди(не будет рабоать если нет подписок)
# r.start_consuming()
# # Остановим прослушивание очереди
# r.stop_consuming()
# # Отвяжем(bind) queue от exchange
# r.queue_unbind("test.queue", "test.exchange", routing_key="")
# # Удалим очередь
# r.queue_delete("test.queue")
# # Удалим exchange
# r.exchange_delete("test.exchange")
# # Закроем соединение
# r.close_connect()