"""
Description: Working with RabbitMQ
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 14.05.2023
Links:
    https://pika.readthedocs.io/en/stable/
    https://www.rabbitmq.com/
    Примеры официальные pika
    https://github.com/pika/pika/tree/main/examples
    Статьи на habr
    https://habr.com/ru/articles/488654/
Comment:
"""

from datetime import date
import datetime
import json
import uuid
from typing import Optional, Union
from pika.exchange_type import ExchangeType
from pika import DeliveryMode
import pika
import pytz


class RabbitMQError(Exception):
    """
    Пользовательский класс для работы с ошибками
    """

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return f"Ошибка клиента RabbitMQ. {self.msg}"


class RabbitMQ:
    """
    ### Класс для подключения и работы с RabbitMQ
    """

    def __init__(self,
                 host: str = 'localhost',
                 port: int = 5672,
                 username: str = 'guest',
                 password: str = 'guest',
                 prefetch_count: int = 0,
                 prefetch_size: int = 0,
                 global_qos: bool = False):

        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self.prefetch_count = prefetch_count
        self.prefetch_size = prefetch_size
        self.global_qos = global_qos
        self._connection = None
        self._channel = None

        self.connect()
        self.qos()

    def connect(self) -> None:
        """
        ### Метод для установления соедниения
        """

        credentials = pika.PlainCredentials(self._username, self._password)
        parameters = pika.ConnectionParameters(host=self._host,
                                               port=self._port,
                                               credentials=credentials)
        self._connection = pika.BlockingConnection(parameters)
        self._channel = self._connection.channel()

    def qos(self) -> None:
        """
        ### Метод для установки настроек качества обслуживания (QoS)
        
        Args:
        - prefetch_count: максимальное количество сообщений, которые могут быть переданы
                          Например, если prefetch_count = 10,то клиент может запросить 
                          10 сообщений в рамках одного соединения
                          и только после того, как ответы на них будут получены,
                          он сможет запросить новые сообщения. 
        - prefetch_size : максимальный размер сообщений, которые могут быть переданы
                          Например, channel.basic_qos(prefetch_size=100000)
                        - prefetch_size равным 100000 (100 кб)
        - global_qos    : Для всех каналов() соединения?
        """
        self._channel.basic_qos(prefetch_count=self.prefetch_count,
                                prefetch_size=self.prefetch_size,
                                global_qos=self.global_qos)

    def close_connect(self) -> None:
        """
        ### Метод для закрытия соединения
        """
        self._connection.close()

    def exchange_exist(self, exchange_name: str) -> bool:
        """
        ### Метод для проверки существования exchange по имени
        Вернет исключение если например у пользователя нет прав на выполнение операции
        
        Args:
        - exchange_name : имя exchange
        """
        try:
            #passive: False выполнить/True проверить существование exchange
            self._channel.exchange_declare(exchange=exchange_name,
                                           passive=True)

        except pika.exceptions.ChannelClosedByBroker:
            return False

        return True

    def exchange_create(self,
                        exchange_name: str,
                        exchange_type: str = ExchangeType.fanout,
                        durable: bool = True) -> pika.frame.Method:
        """
        ### Метод для создания exchange по имени
        Если exchange уже существует, вернет успешный ответ без создания нового exchange.
        
        Args:
        - exchange_name : имя exchange
        - exchange_type : Тип exchange
            - direct, Рассылает на очереди где совпадает Routing key
            - fanout, Рассылает на все очереди, самый быстрый
            - headers, Рассылает на основе заголовков(Headers), содержащихся в сообщениях
            - topic, Тот же Direct, но маршрутизирует сообщения с использованием шаблонов маршрута
        - durable       : Сохранить при перезагрузке RabbitMQ?(Durability)
        """
        return self._channel.exchange_declare(exchange=exchange_name,
                                              exchange_type=exchange_type,
                                              durable=durable)

    def exchange_delete(self, exchange_name: str) -> pika.frame.Method:
        """
        ### Метод для удаления exchange по имени
        
        Args:
        - exchange_name : имя exchange
        """
        return self._channel.exchange_delete(exchange=exchange_name)

    def queue_exist(self, queue_name: str) -> bool:
        """
        ### Метод для проверки существования exchange по имени
        Вернет исключение если например у пользователя нет прав на выполнение операции
        
        Args:
        - queue_name    : имя очереди
        """
        try:
            #passive: False выполнить/True проверить существование queue
            self._channel.queue_declare(queue=queue_name, passive=True)
        except pika.exceptions.ChannelClosedByBroker:
            return False

        return True

    def queue_create(self,
                     queue_name: str,
                     durable: bool = True) -> pika.frame.Method:
        """
        ### Метод для создания queue(очереди) по имени
        Если queue уже существует, вернет успешный ответ без создания нового queue.
        
        Args:
        - queue_name    : имя очереди
        - durable       : Сохранить при перезагрузке RabbitMQ?(Durability)
        """
        return self._channel.queue_declare(queue=queue_name, durable=durable)

    def queue_bind(self,
                   queue_name: str,
                   exchange_name: str,
                   routing_key: str = "") -> pika.frame.Method:
        """
        ### Метод для привязки queue к exchange
        
        Args:
        - queue_name    : имя очереди
        - exchange_name : имя exchange
        - routing_key   : Ключевое слово
        """
        return self._channel.queue_bind(queue=queue_name,
                                        exchange=exchange_name,
                                        routing_key=routing_key)

    def queue_unbind(self, queue_name: str, exchange_name: str,
                     routing_key: str):
        """
        ### Метод для отвязки queue от exchange
        
        Args:
        - queue_name    : имя очереди
        - exchange_name : имя exchange
        - routing_key   : Ключевое слово
        """

        if self.queue_exist(queue_name) and self.exchange_exist(exchange_name):
            self._channel.queue_unbind(queue=queue_name,
                                       exchange=exchange_name,
                                       routing_key=routing_key)

    def queue_message_count(self, queue_name: str) -> Union[int, None]:
        """
        ### Метод для получения колличества сообщений в очереди
        
        Args:
        - queue_name    : имя очереди
        """

        if self.queue_exist(queue_name):
            frame_method = self._channel.queue_declare(queue=queue_name,
                                                       passive=True)
            return frame_method.method.message_count

        return None

    def queue_delete(self, queue_name: str) -> pika.frame.Method:
        """
        ### Метод для удаления queue по имени
        
         Args:
        - queue_name     : имя очереди
        """

        return self._channel.queue_delete(queue=queue_name)

    def publish_message(self,
                        exchange: str,
                        message: str | int | dict,
                        routing_key: str = '',
                        mandatory: bool = True,
                        content_type: str = "application/json",
                        content_encoding: str = "utf-8",
                        headers: Optional[dict] = None,
                        persistant: bool = True,
                        priority: int = 0,
                        correlation_id: Optional[str] = None,
                        reply_to: Optional[str] = None,
                        expiration: Optional[int] = None,
                        message_id: Optional[str] = None,
                        timestamp: Optional[str] = None,
                        message_type: str = "event",
                        user_id: Optional[str] = None,
                        app_id: Optional[str] = None,
                        message_id_list: Optional[list] = None,
                        timezone: str = 'Europe/Moscow') -> str:
        """
        ### Метод для отправки сообщения в exchange
        Примечание: Отсутсвует проверка доставки сообщения
        
        Args:

        - exchange_name     : наименование exchange, в который отправляется сообщение
        - message           : сообщение в формате json
        - routing_key       : ключевое слово, только для direct exchange
        - mandatory         : используется для определения, что если сообщение не может 
                            быть маршрутизировано (не найдена очередь для доставки),
                            то оно должно быть возвращено обратно отправителю,
                            а не отброшено или отправлено на "мертвую" очередь.
        - content_type      : MIME-тип, например "text/plain" или "application/json"
        - content_encoding  : поле, указывающее на кодировку содержимого, например UTF-8
        - headers           : пользовательские заголовки, например {'key1': 'value1'}
        - delivery_mode     : поле, указывающее, нужно ли делать сообщение 
                            Persistent(сохранять на диск) или Transient(в опертивной памяти).
                            В Случае сбоя или перезапуска сообщение будет востановленно
        - priority          : приоритет сообщения. По умолчанию - 0, выше 0 высокий, ниже 0 - низкий
        - correlation_id    : уникальный идентификатор для связывания сообщения с ответом
        - reply_to          : имя очереди или exchange, в которую нужно отправить ответ
        - expiration        : время жизни сообщения в очереди (в миллисекундах)
        - message_id        : уникальный идентификатор сообщения, который генерируется отправителем.
        - timestamp         : поле, указывающее на время создания сообщения (только UnixTime)
        - message_type      : поле, которое позволяет указать тип сообщения
            - request         Запросы 
            - response        Ответы
            - event           Событийные сообщения
            - execution       Выполнение
            - notifications   Оповещения
            - error           Сообщения об ошибках
            - data            Передача данных
        - user_id           : идентификатор пользователя, который отправил сообщение
        - app_id            : идентификатор приложения, которое отправило сообщение
        - message_id_list   : список парамметров, которые идентифицируют сообщение, должны быть уникальны
        - timezone          : pytz таймзона датирования сообщений, по умолчанию - 'Europe/Moscow'

        Out:
        - uuid : Уникальный uuid
        """

        if message_id is None:
            message_id = RabbitMQ.get_uuid(message_id_list)

        body = json.dumps(message,
                          ensure_ascii=False,
                          indent=4,
                          cls=CustomJSONEncoder)

        tz_date = pytz.timezone(timezone)
        now = datetime.datetime.now(tz_date)
        #timestamp = now.isoformat()
        timestamp = int(now.timestamp())

        headers_combined = {
            "date": now.isoformat(),
        }
        if headers:
            headers_combined.update(headers)

        properties = pika.BasicProperties(
            content_type=content_type,
            content_encoding=content_encoding,
            headers=headers_combined,
            delivery_mode=DeliveryMode.Persistent
            if persistant else DeliveryMode.Transient,
            priority=priority,
            correlation_id=correlation_id,
            reply_to=reply_to,
            expiration=expiration,
            message_id=message_id,
            timestamp=timestamp,
            type=message_type,
            user_id=user_id,
            app_id=app_id)

        self._channel.basic_publish(exchange=exchange,
                                    routing_key=routing_key,
                                    body=body,
                                    properties=properties,
                                    mandatory=mandatory)
        return message_id

    def get_message(self, queue_name: str, auto_ack: bool = False) -> tuple:
        """
        ### Метод для получения сообщения из queue
        
        Args:
        - auto_ack: отправить подтверждение получения сообщения автоматически или нет
                    True - удалит(подтвердить получение) из очереди
                    False - оставит сообщение, нужно будет удалить (подтвердить получение)
                    при помощи .basic_ack(method.delivery_tag)
        """

        return self._channel.basic_get(queue_name, auto_ack)

    def get_message_all(self,
                        queue_name: str,
                        auto_ack: bool = True) -> Union[list, None]:
        """
        ### Метод для получения всех сообщений из очереди
        
        Args:
        - auto_ack: отправить подтверждение получения сообщения автоматически или нет
                    True - удалит(подтвердить получение) из очереди
                    False - оставит сообщение, нужно будет удалить (подтвердить получение)
                    при помощи .basic_ack(method.delivery_tag)
                    
        Если парамметр auto_ack: bool = False, то необходимо отправить подтверждение о получении,
        это можно сделать либо:
            - для каждого сообщения
                r = RabbitMQ()
                message_all = (r.get_message_all(queue_name, auto_ack=False))
                r.ack_message(delivery_tag=message_all[0]['method_frame'].delivery_tag)
            - для всех
                r = RabbitMQ()
                message_all = (r.get_message_all(queue_name, auto_ack=False))
                r.ack_message(delivery_tag=len(message_all), multiple=True)
        """
        messages = []
        message_count = self.queue_message_count(queue_name)

        if (message_count is not None) and (message_count > 0):
            for i in range(message_count):
                method_frame, header_frame, body = self.get_message(
                    queue_name, auto_ack=auto_ack)

                message = {
                    'method_frame': method_frame,
                    'header_frame': header_frame,
                    'body': body
                }

                messages.append(message)

            return messages

        return None

    def ack_message(self,
                    delivery_tag: int = 0,
                    multiple: bool = False) -> None:
        """
        ### Метод для подтверждения получения сообщения(удалит из очереди RabbitMQ)
        
        Args:
        - delivery_tag  : уникальный номер для каждого сообщения, 
                        которое было доставлено на потребитель RabbitMQ. 'channel.basic_consume'
        - multiple      : используется для подтверждения одного или нескольких сообщений
                        True - все сообщения с 'delivery_tag' меньше или равным 
                        указанному параметру `delivery_tag` будут подтверждены.
                        False - одно сообщение
        """

        self._channel.basic_ack(delivery_tag, multiple)

    def consume(self,
                queue_name: str,
                callback,
                auto_ack: bool = False,
                exclusive: bool = False,
                consumer_tag: str = None,
                arguments: dict = None) -> str:
        """
        ### Метод для регистрации обработчика сообщений, для прослушивания очереди

        Args:
        - callback      : функция-обработчик, вызывается каждый раз как будет полученно сообщение
        - auto_ack      : указывает, должен ли брокер автоматически
                        подтверждать получение сообщения
        - exclusive     : этот параметр позволяет создать временную очередь,
                        которая будет удалена при закрытии соединения
        - consumer_tag  : строка, которая идентифицирует подписавшегося на очередь потребителя
                        Если параметр не указан, то брокер сам назначит уникальный тег
                        Если нужно отписаться от очереди, используется channel.basic_cancel(),
                        нужно будет указать этот тег
        - argument      : словарь, который позволяет передавать произвольные аргументы
        
        Out:
        - consumer_tag : строка, которая идентифицирует подписавшегося на очередь потребителя
        """

        if self.queue_exist(queue_name):
            tag = self._channel.basic_consume(queue=queue_name,
                                              on_message_callback=callback,
                                              auto_ack=auto_ack,
                                              exclusive=exclusive,
                                              consumer_tag=consumer_tag,
                                              arguments=arguments)
            return tag

    def start_consuming(self):
        """
        ### Метод для запуска приема сообщений из очереди
        """
        try:
            self._channel.start_consuming()
        except KeyboardInterrupt:
            self._channel.stop_consuming()

    def stop_consuming(self) -> None:
        """
        ### Метод для остановки приема сообщений из очереди
        """
        self._channel.stop_consuming()

    def consume_unsubscribe(self, consumer_tag: str) -> str:
        """
        ### Метод для отписки обработчика сообщений от прослушивания очереди

        Args:
        - consumer_tag : строка, которая идентифицирует подписавшегося на очередь потребителя
        """

        self._channel.basic_cancel(consumer_tag=consumer_tag)

    def create_structure(self,
                         exchange_name: str,
                         queues: list,
                         exchange_type: str = "fanout",
                         durable: bool = True,
                         routing_key: str = '') -> None:
        """
        ### Метод для создания exchange,queues(очередей) и их привязки(bind)
        Индепотента. Работает только для exchange_type - fanout и direct

        Args:
        - exchange_name : Название exchange
        - queues        : Список очередей, которые должны быть созданы
        - exchange_type : Тип exchange
            - fanout, Рассылает на все очереди, самый быстрый
            - direct, Рассылает на очереди где совпадает Routing key
        - durable       : Сохранить при перезагрузке RabbitMQ
        - routing_key   : Ключевое слово, только для direct
        """

        # Создадим exchange(роутер)
        self.exchange_create(exchange_name=exchange_name,
                             exchange_type=exchange_type,
                             durable=durable)
        for queue in queues:
            # Создадим queue
            self.queue_create(queue_name=queue, durable=durable)
            # Свяжем(bind) queue и exchange
            self.queue_bind(exchange_name=exchange_name,
                            queue_name=queue,
                            routing_key=routing_key)

    @classmethod
    def get_uuid(cls, data_calc: list = None) -> str:
        """
        ### Метод для получения uuid
        Если указан data_calc, то набор данных должен быть уникальный!
        
        Args:
        - data_calc : Список парамметров, которые идентифицируют сообщение

        Out:
        - uuid : Уникальный uuid
        """
        if data_calc:
            str_calc = ""
            for item in data_calc:
                str_calc = str_calc + str(item)

            return str(uuid.uuid5(uuid.NAMESPACE_DNS, str_calc))
        return str(uuid.uuid4())
        
    @classmethod
    def filter_dict(cls, dict_in: dict, keys: list) -> dict:
        """
        ### Метод выводит словарь, или json, только с указаными ключами
        Если ключа в исходном словаре нет, он пропускается
        
        Args:
        - dict_in   : Входящий словарь или json
        - keys      : Список ключей
        """
        result = {}
        for key in keys:
            if key in dict_in:
                result[key] = dict_in[key]
        return result

class CustomJSONEncoder(json.JSONEncoder):
    """ 
    Пользовательский класс, для декодирования в JSON
    """

    def default(self, o):
        try:
            if isinstance(o, uuid.UUID):
                return o.hex
            if isinstance(o, date):
                return o.isoformat()
            iterable = iter(o)
        except TypeError:
            pass
        else:
            return list(iterable)
        return json.JSONEncoder.default(self, o)
