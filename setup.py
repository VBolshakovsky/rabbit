from setuptools import find_packages, setup

setup(
    name='rabbit_client',
    packages=find_packages(),
    version='0.1.0',
    description='Rabbit client',
    author='V.Bolshakov',
)